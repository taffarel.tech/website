---
title: Demandas restantes
date: 2023-02-05
---

## Futuras versões

## Tornando mais acessível e colaborativo
- [ ] Fazer acessibilidade do site enquanto estudo sobre isso, pedir ajuda a alguém
- [ ] Adicionar funcionalidade de se mudar o idioma do site a partir do Header
- [ ] Definir padrão de yaml nos posts, documentar e adicionar validação aos testes
- [ ] Adicionar funcionalidade de editar páginas no GitLab

---

## Futuro
- [ ] Consumir API do github para pegar imagem pelo ID
- [ ] Olhar Trello com sites de inspiração e tools que estão salvas
- [ ] Ajustar cores do site, dar um pouco mais de vida
- [ ] Adicionar componentes Svelte no markdown
  - [ ] Fazer componentes para notas de info, tip, danger e colocar no markdown
  - [ ] Adicionar suporte a diagramas de eletrônica
  - [ ] Desmos
  - [ ] Youtube
  - [ ] Circuitos eletrônicos - Circuitverse
  - [ ] Circuitos analógicos - Falstad
  - [ ] Mermaid.js
  - [ ] Lib para gráfico?
- [ ] Adicionar imagens nos posts
- [ ] Modo leitura firefox
- [ ] Modo impressão/geração de PDF
- [ ] Fazer um overlay com spinner enquanto espera o css da página
- [ ] Recurso de newsletter
- [ ] Adicionar comentários usando utterances
- [ ] Adicionar ads? É possível?
- [ ] Instaview do Telegram
- [ ] Fazer botão para voltar baseado no histórico do navegador (breadcumb?)
- [ ] Recurso para copiar conteúdo de códigos
- [ ] Tema claro?
- [ ] Ao copiar um trecho que não seja código, colocar link para post
- [ ] Ao selecionar um trecho do blog, colocar um balão em cima pra compartilhamento
- [ ] Highlight em trechos de código
- [ ] API pagination options (Our API is currently missing pagination options, which could be very needed depending on the number of posts.)
- [ ] API Post (Another possible future enhancement would be adding the post’s content to the returned JSON, which we don’t do currently. The “resolver” function offers a `default.render` method for that, if you so choose. (We saw it previously, in the `src/routes/[slug]/+page.js` file.))
