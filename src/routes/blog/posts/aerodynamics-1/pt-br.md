---
title: 'Aerodinâmica'
description: Algumas reflexões introdutórias
date: 2023-03-30
categories:
  - PEE32
  - Resumos
author:
  id: 18634201
  name: Maurício Taffarel
  user: taffarel55
time: 20
published: true
---

# 1. Aerodinâmica: algumas reflexões introdutórias

> 📚 Resumo feito enquanto eu estudava o livro Fundamentals of Aerodynamics, Sixth Edition de John D. Anderson, Jr., ISBN 978-1-259-12991-9.
>
> - Os conceitos aqui apresentados é resumo de equações de alguns capítulos do livro original, com pesquisas adicionais na internet. As informações foram reescritas com minhas próprias palavras e as vezes com ordem diferente do livro e estas informações não são suficientes para substituir a obra original.
>
> - O objetivo é auxiliar na compreensão do livro original, mas não o substituir. É recomendável a leitura completa do livro para um entendimento mais profundo do assunto.
>
> - Este resumo omite deliberadamente detalhes intelectuais e informações extras presentes no livro original. O objetivo é fornecer uma visão geral concisa apenas dos conceitos principais relacionados a aerodinâmica.

## 1.1 Importância da Aerodinâmica: Exemplos históricos

Apresenta alguns exemplos históricos e comenta sobre os trabalhos de Isaac **Isaac Newton (1642-1727)** na análise de fluidos, as dificuldades envolvidas comparadas com corpos sólidos. Também comenta os experimentos e resultados conduzidos por diversos nomes: **Jean LeRond d’Alembert (1717-1783)**, **Leonhard Euler (1707–1783)**, **Otto Lilienthal (1848–1896)**, **Samuel Pierpont
Langley (1934–1906)**, entre outros.

## 1.2 Aerodinâmica: Classificação e Objetivos Práticos

É definido o que é um fluido, seja ele um líquido ou um gás. Quando uma tensão de cisalhamento é aplicada a um fluido ele sofre uma deformação progressiva contínua proporcional à esta tensão de cisalhamento.

Considerando um escoamento do fluido no plano $xy$ e escoamento no sentido de $\hat{x}$, como mostra a imagem abaixo:

![Pressão laminar de um fluido entre duas placas. Atrito entre o fluido e a  superfície móvel causa a torsão do fluido. A força necessária para essa  ação é a medida da viscosidade do fluido.](https://upload.wikimedia.org/wikipedia/commons/a/a4/Laminar_shear.png)

A variação da velocidade ao longo de $\hat{y}$ é proporcional à tensão de cisalhamento $\tau$, a constante de proporcionalidade (caso seja um fluido Newtoniano) é a viscosidade $\mu$:

$$
\tau = \mu \frac{\partial u}{\partial y}
$$

> Esta tensão de cisalhamento pode aparecer devido a forças friccionais quando há um gradiente de velocidade ao longo de um escoamento.

## 1.3 Roteiro para este capítulo

Apresenta um útil _roadmap_ para ler o livro e saber onde você está, onde vai chegar e o que aprendeu.

## 1.4 Algumas variáveis aerodinâmicas fundamentais

Neste capítulo é apresentado algumas variáveis aerodinâmicas:

### Pressão $p$:

Relação entre uma determinada [força](https://pt.wikipedia.org/wiki/Força) normal e sua área de distribuição e pode variar com o ponto do espaço de avaliação:

$$
p = lim_{dA\rightarrow0} \frac{dF}{dA}
$$

### Densidade $\rho$:

Mede o grau de [concentração](https://pt.wikipedia.org/wiki/Concentração) de massa em determinado volume e também é uma propriedade pontual e varia ao longo do fluido:

$$
\rho = lim_{dv\rightarrow0} \frac{dm}{dv}
$$

### Temperatura $T$:

**Temperatura** é uma [grandeza física](https://pt.wikipedia.org/wiki/Grandeza_física) que mede a [energia cinética](https://pt.wikipedia.org/wiki/Energia_cinética) média de cada [grau de liberdade](<https://pt.wikipedia.org/wiki/Graus_de_liberdade_(física)>) de cada uma das partículas de um sistema em [equilíbrio térmico](https://pt.wikipedia.org/wiki/Equilíbrio_térmico).

$$
T = \frac{\partial U(S,V,N)}{\partial S}
$$

### Velocidade de fluxo $\mathbf{V}$:

**Velocidade** é um vetor que representa a velocidade infinitesimal de um fluido em um ponto no espaço e um determinado elemento do fluido escoa ao longo de um caminho definido conhecido como **linha de fluxo**.

⏳ Continua...
