---
title: Projeto de Finanças para estudar DevOps
description: Desenvolva uma aplicação útil e prática para gerenciamento de finanças pessoais enquanto aprende as principais ferramentas e tecnologias de DevOps para aprimorar o desenvolvimento, a entrega e a manutenção da aplicação.
date: 2023-03-12
time: 15
categories:
  - DevOps
  - Learning
author:
  id: 18634201
  name: Maurício Taffarel
  user: taffarel55
---

## 💬 Introdução

<!-- Objetivo do projeto -->

### 🎯 Objetivo do projeto

O projeto de finanças tem como principal objetivo a prática e aprendizado de tecnologias e práticas de DevOps, como a utilização de containers, ferramentas de gerenciamento de configuração e automação de deploy. Dessa forma, o projeto é desenvolvido do zero, buscando a implementação de boas práticas e a utilização das tecnologias mais atuais.

A aplicação em si é uma aplicação web, que busca oferecer uma interface intuitiva e fácil de usar para o registro de movimentações financeiras e um resumo das finanças do usuário. O projeto foi desenvolvido em parceria com um amigo, já que a contribuição no DevOps é fundamental para a implementação do projeto.

> Com o projeto de finanças, o usuário pode registrar movimentações financeiras, como entradas e saídas, além de ter acesso a um resumo de suas finanças. Para obter essas informações, a aplicação se integra com uma API, acessada através do backend da aplicação. Dessa forma, a aplicação oferece uma solução completa para o gerenciamento financeiro pessoal.

### 💻 Tecnologias utilizadas

Abaixo estão as principais tecnologias utilizadas no projeto, e para cada uma delas será explicado em detalhes o que foi feito. Caso prefira, você pode acessar diretamente o conteúdo que deseja.

- [**🗄️ Backend**](#-desenvolvimento-da-aplicação)
  - Desenvolvimento da aplicação com as tecnologias: **Node.js**, **Express** e **json-server**.
  - Funcionamento geral da API
- [**💅 Frontend**](#-desenvolvimento-da-aplicação)
  - Desenvolvimento do front utilizando o **React**.
  - Funcionamento geral do frontend
- [**🧪 Testes**](#-testes)
  - Criação de testes unitários no backend para garantir a qualidade do código.
  - Criação de testes unitários no frontend para garantir a qualidade do código.
- [**🐋 Docker**](#-docker)
  - Criação de Dockerfiles para o backend e frontend, visando facilitar o processo de build e deploy.
  - Utilização de multi-stage build para otimizar o tamanho da imagem gerada.
  - Aprendizado de técnicas e boas práticas para a utilização do Docker.
- [**🦊 GitLab**](#-gitlab)
  - Criação do projeto em outra plataforma para versionamento do código fonte.
  - Utilização do registry para armazenamento de imagens docker.
  - Implementação de pipeline de CI/CD com stages para automatizar o processo de testes, build e deploy.
  - Criação e uso de variáveis no pipeline para flexibilidade e segurança.
  - Utilização de Issues em Boards para gerenciamento de tarefas.
- [**☸️ Kubernetes**](#%EF%B8%8F-kubernetes)
  - Gerenciamento de um cluster local para testes e um remoto para deploy em produção.
  - Práticas com os comandos kubectl para gerenciamento de recursos.
  - Utilização do Deployment para gerenciamento de replicas do frontend e backend.
  - Utilização do ReplicaSet para garantir alta disponibilidade e escalabilidade da aplicação.
  - Utilização do Pod para execução de containers.
  - Utilização de Services ClusterIP e LoadBalancer para gerenciamento de acessos à aplicação.
- [**🟩 Nginx**](#-nginx)
  - Configuração básica nginx.conf para servir como proxy reverso para o frontend e backend.
  - Passagem de solicitações para o proxy server para garantir a comunicação correta entre as camadas.
- [**🏗 Terraform**](#-terraform)
  - Utilização do Terraform para provisionar a infraestrutura necessária para o projeto.
  - Leitura de documentação do Terraform para entender a sua sintaxe e melhores práticas.
  - Utilização do Terraform para provisionar recursos na Digital Ocean.
- [**🌊 Digital Ocean**](#-digital-ocean)
  - Familiarização com o Cloud Provider utilizado para hospedar a aplicação.
  - Verificação de métricas no terminal web para monitoramento da saúde da aplicação.
- [**🌐 Configuração de DNS**](#-configuração-de-dns)
  - Adicionar registro A para criar um subdomínio para acesso à aplicação.
  - Apontar para nameservers para redirecionar o tráfego do domínio para o endereço IP do cluster.

## 👨🏻‍💻 Desenvolvimento da Aplicação

- Escrevendo código-fonte
- Testes unitários

A seguir será explicado como o projeto [finances](https://gitlab.com/devops-study-br/finances) foi desenvolvido. Se você deseja verificar o código em sua máquina, pode executar o seguinte comando se tiver o git instalado:

```bash
git clone https://gitlab.com/devops-study-br/finances.git
```

Ao clonar o projeto [finances](https://gitlab.com/devops-study-br/finances) e acessar sua pasta, você terá acesso à seguinte estrutura:

```bash
❯ cd finances
❯ tree -L 2 -I node_modules -I assets
.
├── back
│   ├── collection
│   ├── coverage
│   ├── db.json
│   ├── Dockerfile
│   ├── junit.xml
│   ├── node_modules
│   ├── package.json
│   ├── package-lock.json
│   ├── README.md
│   └── src
├── front
│   ├── build
│   ├── coverage
│   ├── Dockerfile
│   ├── junit.xml
│   ├── nginx.conf
│   ├── node_modules
│   ├── package.json
│   ├── package-lock.json
│   ├── public
│   ├── README.md
│   └── src
├── k8s
│   ├── backend-deployment.yml
│   ├── backend-service.yml
│   ├── frontend-deployment.yml
│   └── frontend-service.yml
├── LICENSE
├── README.md
└── terraform
    ├── get_kube_token.sh
    ├── kube_config.b64
    ├── kube_config.yml
    ├── main.tf
    ├── terraform.tfstate
    ├── terraform.tfstate.backup
    └── terraform.tfvars

13 directories, 25 files
```

O repositório contém não apenas os arquivos do frontend e do backend, mas também seus arquivos para testes, bem como pastas relacionadas ao provisionamento via Terraform e arquivos de manifesto Kubernetes. Abaixo, vamos discutir cada um desses elementos em mais detalhes.

### 🗄️ Backend

Durante o desenvolvimento da aplicação, utilizamos as tecnologias **Node.js**, **Express** e **json-server** para criar o backend. Não entrarei em detalhes sobre como criar um backend, mas caso você queira aprender mais sobre essas tecnologias, segue abaixo os links:

- <a href="https://nodejs.org/pt-br/about/" target="_blank" rel="noreferrer">Node.js</a>
- <a href="https://expressjs.com/pt-br/" target="_blank" rel="noreferrer">Express</a>

O **json-server** foi utilizado para criar uma API fake no backend da aplicação. Ele nos permitiu simular o comportamento de uma API real sem a necessidade de criar um banco de dados. Como este projeto é o primeiro e não requer um banco de dados de alta disponibilidade, essa foi uma solução simples e eficaz.

> Caso você queira saber mais sobre essa biblioteca que é capaz de gerar uma API fictícia <a href="https://www.npmjs.com/package/json-server" target="_blank" rel="noreferrer">clique aqui</a>

No arquivo `back/package.json`, os scripts definidos na seção "scripts" são comandos que podem ser executados pelo NPM (Node Package Manager) através do terminal.

```bash
❯ cat back/package.json | grep -A 2 scripts

  "scripts": {
    "start": "node src/index.js",
    "dev": "nodemon src/index.js",
```

Podemos ver através do comando anterior, que existem definidos os scripts "start" e "dev". O script "start" é responsável por executar a aplicação em produção. Quando você executa o comando npm run start, o NPM irá rodar o comando `node src/index.js`, que irá iniciar a aplicação.

Já o script "dev" é utilizado durante o desenvolvimento da aplicação. Quando você executa o comando `npm run dev`, o NPM irá rodar o comando `nodemon src/index.js`. O <a href="https://nodemon.io/" target="_blank" rel="noreferrer">nodemon</a> é uma ferramenta que reinicia automaticamente a aplicação toda vez que algum arquivo do código-fonte é modificado, o que é muito útil durante o desenvolvimento.

> Em resumo, o comando `npm run start` inicia a aplicação em produção e `npm run dev` inicia a aplicação em modo de desenvolvimento com o nodemon.

#### Funcionamento da API

A API do backend é a interface que possibilita a comunicação entre o front-end e o banco de dados. Em outras palavras, ela é responsável por receber as requisições do usuário e retornar as respostas adequadas. Através das rotas definidas no código, é possível realizar operações de CRUD (Create, Read, Update e Delete) no banco de dados, como listar todas as transações, criar uma nova transação, atualizar ou excluir uma transação existente.

Além disso, as rotas também permitem que sejam realizadas operações específicas em determinadas transações, como visualizar os detalhes de uma transação específica. Toda a lógica da API é implementada através dos controllers, que são responsáveis por realizar as operações nos dados e retornar as respostas adequadas para as requisições. As rotas disponíveis são:

- `GET /transactions`: lista todas as transações registradas.
- `POST /transactions`: cria uma nova transação.
- `DELETE /transactions`/:id: deleta uma transação especificada pelo parâmetro id.
- `PUT /transactions/:id`: atualiza todas as informações de uma transação especificada pelo parâmetro id.
- `PATCH /transactions/:id`: atualiza uma única propriedade de uma transação especificada pelo parâmetro id.
- `GET /transactions/:id`: exibe os detalhes de uma transação especificada pelo parâmetro id.

Ao implementar uma API bem projetada, podemos fornecer uma maneira eficiente e segura de acessar e gerenciar dados de um aplicativo. Com a popularidade crescente de aplicativos web e móveis, ter uma API bem desenvolvida pode ser crucial para o sucesso de um produto. Com as ferramentas e tecnologias disponíveis hoje, desenvolver uma API robusta e escalável é mais fácil do que nunca.

### 💅 Frontend

No projeto de frontend, o React foi utilizado para criar a interface do usuário. O código foi desenvolvido no passado, durante o período em que eu estava estudando React. É importante mencionar que este foi um dos meus primeiros projetos utilizando a biblioteca, então há uma quantidade significativa de [prop drilling](https://kentcdodds.com/blog/prop-drilling) e códigos mal estruturados. Mesmo assim, foi uma experiência valiosa para o meu desenvolvimento como programador e estou sempre em busca de aprimorar minhas habilidades.

![Frontend da aplicação, uma página web com uma tabela com transações, um botão para filtrar e uma tabela resumo com os valores de entradas e saídas das transações](./assets/dindin.png)

O frontend da aplicação consiste em uma página web que apresenta uma tabela com todas as transações cadastradas no backend. Além disso, a página conta com um botão para filtrar as transações por tipo (entrada ou saída) e uma tabela resumo que exibe os valores totais de entradas e saídas.

A tabela de transações é atualizada automaticamente sempre que ocorre uma alteração no backend, o que garante que o usuário tenha sempre as informações mais atualizadas. Já o botão de filtro permite que o usuário visualize apenas as transações de um determinado tipo, tornando a navegação na página mais eficiente.

Por fim, a tabela resumo apresenta de forma clara e objetiva as informações financeiras mais relevantes para o usuário, como o total de entradas e saídas, permitindo que ele tenha uma visão geral do seu fluxo financeiro.

Assim como no backend, como o frontend também é escrito em nodejs, ele possui um arquivo `front/package.json`, os scripts definidos na seção "scripts" são comandos que podem ser executados pelo NPM (Node Package Manager) através do terminal.

```bash
❯ cat front/package.json| grep -A 6 '"scripts"'
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --watchAll",
    "eject": "react-scripts eject",
    "test:ci": "npm run test -- --testResultsProcessor=\"jest-junit\" --watchAll=false --ci --coverage"
  },
```

O script "start" é responsável por inicia o servidor de desenvolvimento do React, permitindo que o usuário visualize a aplicação no navegador. Este script é executado com o comando `npm run start`. Já o script "build", realiza a build da aplicação React, gerando uma versão otimizada para produção. Este script é executado com o comando `npm run build`.

O script "eject", ejeta o app do **Create React App** (ferramenta que foi utilizada para criar o projeto React e agrupa diversas ferramentas) é um processo irreversível e, uma vez feito isso, não há como voltar atrás. Ao executar este comando, o Create React App expõe todas as configurações e dependências utilizadas para gerar a aplicação React, permitindo uma maior customização e configuração do projeto. Este script é executado com o comando `npm run eject`.

Os scripts de testes serão descritos no próximo tópico.

> Para o uso desta aplicação, usaremos o `npm run build` para gerar os arquivos do site otimizados para produção.

### 🧪 Testes

Para garantir a qualidade do código e evitar possíveis erros, foram criados testes unitários no backend, para executar estes testes, temos outros dois scripts para esta finalidade.

```bash
❯ cat back/package.json | grep -A 5 scripts

  "scripts": {
    "start": "node src/index.js",
    "dev": "nodemon src/index.js",
    "test": "jest --watchAll",
    "test:ci": "npm run test -- --testResultsProcessor=\"jest-junit\" --watchAll=false --ci --coverage"
  },
```

O script "test" é responsável por executar os testes unitários escritos para o backend, utilizando o framework Jest. Ao ser executado, o Jest inicia o modo de observação (watchAll) para executar os testes novamente sempre que houver alterações nos arquivos de teste.

> O script "dev" e o script "test" têm uma relação importante em comum: ambos monitoram o código-fonte em busca de alterações. Enquanto o "dev" reinicia a aplicação sempre que um arquivo é modificado, o "test" executa novamente os testes unitários escritos para o backend sempre que houver alterações nos arquivos de teste.
>
> Essa relação é fundamental para garantir que tanto a aplicação quanto os testes estejam sempre atualizados e em pleno funcionamento. Sem ela, seria necessário reiniciar manualmente a aplicação ou executar os testes unitários a cada pequena alteração no código-fonte, o que seria impraticável e consumiria muito tempo.

E por último, o script "test:ci" é usado para executar os testes unitários em um ambiente de integração contínua (CI). Ele usa o mesmo comando do test, mas com algumas opções adicionais para gerar relatórios de cobertura de testes e um arquivo de resultados no formato JUnit para ser consumido por ferramentas de CI/CD. O parâmetro `--watchAll=false` garante que o Jest execute todos os testes apenas uma vez e, em seguida, encerre o processo.

Os testes que estes dois últimos scripts executam foram criados testes unitários no backend. Esses testes foram escritos com o framework Jest e abrangiam os endpoints GET da API.

Embora o backend seja um projeto simples, foram criados testes para garantir sua qualidade e funcionalidade. Abaixo está o código de exemplo de testes, que foram escritos utilizando a biblioteca Jest e a ferramenta Supertest para realizar chamadas HTTP à API. Esses testes foram escritos com o objetivo de ilustrar como poderiam ser criados testes para o backend, cobrindo casos de uso básicos, como a recuperação de transações e a verificação de seus detalhes.

```js
❯ cat back/src/__tests__/transactions.test.js
const request = require("supertest");
const app = require("../server.js");
const db = require("../db.js");
const initialTransactions = require("../initialTransactions");

let dbServer;

beforeAll(() => {
  dbServer = db.listen(3334);
});

afterAll(() => {
  dbServer.close();
});

describe("Testando endpoints", () => {
  test("GET /transactions", async () => {
    const response = await request(app).get("/transactions");
    const data = response.body;

    expect(data).toEqual(initialTransactions);
  });

  test("GET /transactions/1", async () => {
    const response = await request(app).get("/transactions/1");
    const data = response.body;

    expect(data).toEqual(initialTransactions[0]);
  });
});
```

O "describe" agrupa vários testes relacionados, nesse caso, todos os testes estão relacionados aos endpoints da API. Cada "test" dentro do bloco "describe" é um teste de unidade para um endpoint específico. O primeiro teste verifica se o endpoint "/transactions" retorna a lista inicial de transações. O segundo teste verifica se o endpoint "/transactions/1" retorna a primeira transação na lista inicial.

Por fim, o "expect" é uma função de asserção do Jest que verifica se o resultado obtido é igual ao resultado esperado. Dessa forma, numa aplicação real, conseguimos detectar e corrigir erros antes mesmo da aplicação ser disponibilizada aos usuários.

Os testes unitários do frontend foram escritos com o framework React Testing Library. Eles cobriam todos os componentes e funcionalidades da aplicação. Com esses testes, conseguimos garantir que a interface do usuário estivesse funcionando corretamente e que possíveis erros fossem detectados e corrigidos durante o desenvolvimento.

```bash
❯ cat front/package.json| grep -A 6 '"scripts"'
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --watchAll",
    "eject": "react-scripts eject",
    "test:ci": "npm run test -- --testResultsProcessor=\"jest-junit\" --watchAll=false --ci --coverage"
  },
```

O script "test" executa todos os testes encontrados no projeto que tenham o sufixo `.test.js` utilizando o `@testing-library` e inicia um modo de observação, em que os testes são executados novamente sempre que houver alterações no código-fonte dos testes. Já o "test:ci", executa os os mesmos testes utilizando o @testing-library e gera um relatório em formato JUnit que pode ser utilizado por ferramentas de integração contínua. Além disso, desabilita o modo de observação (--watchAll=false) e gera um relatório de cobertura de código (--coverage).

> Desta forma a aplicação pode ser testada em modo de desenvolvimento e também pode ser usado no pipeline do CI

Abaixo está um exemplo de um teste para o frontend:

```js
❯ cat front/src/components/Resume/index.test.js
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";
import Resume from "./index.js";

describe("<Resume/>", () => {
  let container;

  beforeEach(() => {
    container = render(<Resume />).container;
  });

  test("Verificação do título do Resumo", () => {
    const titleElement = screen.getByText(/Resumo/i);
    expect(titleElement).toBeInTheDocument();
  });

  test("Verifica texto Entradas e Saídas", () => {
    const entradas = screen.getByText(/Entradas/i);
    const saidas = screen.getByText(/Saídas/i);
    expect(entradas).toBeInTheDocument();
    expect(saidas).toBeInTheDocument();
  });
});
```

Esse código é um teste unitário que verifica a renderização do componente Resume em uma aplicação React. Ele verifica se o componente contém o título "Resumo" e se há texto "Entradas" e "Saídas". Ele usa a biblioteca @testing-library/react para realizar esses testes.

![Componente Resumo](./assets/resume.png)

Em resumo, os testes são essenciais tanto para o desenvolvimento do frontend quanto do backend, pois ajudam a garantir que o código está funcionando corretamente e evitam a introdução de erros durante o processo de desenvolvimento. Além disso, os testes também permitem que os desenvolvedores detectem erros rapidamente e os corrijam antes de enviar o código para produção. Com a ajuda de frameworks de testes e bibliotecas, é possível criar testes automatizados que são executados de forma consistente e confiável, fornecendo assim uma camada adicional de segurança e confiança no software desenvolvido.

## 🐋 Docker

O Docker é uma tecnologia de virtualização que permite empacotar um aplicativo com todas as suas dependências em um contêiner isolado, que pode ser executado em qualquer ambiente. Isso torna a implantação de aplicativos mais fácil, consistente e escalável.

> Falar sobre o Dockerfile

```docker
❯ cat back/Dockerfile
# ==== CONFIGURE =====

# Use a Node base image
FROM node:18-alpine as build

# Set the working directory to /usr/src/app inside the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./


# ==== INSTALL =====
# Install dependencies
RUN npm install
# Copy files after install dependencies
COPY . .

# ==== RUN =======

# Expose the port on which the app will be running
EXPOSE 3333

# Start the app
CMD ["npm", "run", "start"]
```

A primeira etapa define a imagem base do Docker que será usada para criar a imagem do aplicativo, neste caso, o `node:18-alpine`. Isso significa que o Docker usará uma imagem do Node.js baseada no Alpine Linux como a imagem base para o aplicativo. A próxima etapa é configurar o ambiente para o aplicativo. Neste caso, estamos definindo o diretório de trabalho dentro do contêiner como `/usr/src/app` e copiando os arquivos `package*.json` para lá.

Copiar o package*.json antes de instalar as dependências é uma boa prática para otimizar o processo de construção da imagem Docker. Ao copiar os arquivos package*.json separadamente, podemos aproveitar o cache do Docker para a camada de instalação de dependências, evitando ter que baixar todas as dependências novamente caso haja uma modificação em qualquer outro arquivo do projeto.

Dessa forma, quando rodamos o comando docker build novamente após modificar o código-fonte do projeto, o Docker utilizará o cache das camadas que não foram afetadas pelas alterações, o que torna o processo de construção mais rápido. Essa prática é especialmente importante em projetos grandes com muitas dependências.

Em seguida, são instaladas as dependências do Node.js com o comando `RUN npm install`. Em seguida, todos os arquivos da aplicação são copiados para dentro do contêiner. Por fim, a porta 3333 é exposta e o comando npm run start é executado para iniciar o servidor da aplicação.

> Essa porta será utilizada para realizar as configurações necessárias para permitir a comunicação entre o backend e o frontend.

Para construir a imagem Docker do backend, você pode executar o seguinte comando na raiz do diretório do backend:

```bash
❯ docker build -t backend ./back
[+] Building 20.1s (10/10) FINISHED
 => [internal] load build definition from Dockerfile                       0.1s
 => => transferring dockerfile: 508B                                       0.0s
 => [internal] load .dockerignore                                          0.1s
 => => transferring context: 2B                                            0.0s
 => [internal] load metadata for docker.io/library/node:18-alpine          0.0s
 => [internal] load build context                                          1.9s
 => => transferring context: 31.22MB                                       1.8s
 => [1/5] FROM docker.io/library/node:18-alpine                            0.0s
 => CACHED [2/5] WORKDIR /usr/src/app                                      0.0s
 => [3/5] COPY package*.json ./                                            0.4s
 => [4/5] RUN npm install                                                 11.7s
 => [5/5] COPY . .                                                         3.8s
 => exporting to image                                                     2.1s
 => => exporting layers                                                    2.1s
 => => writing image sha256:f62cf160fa78b81b6b8a3df61447774b5c39277249fbd  0.0s
 => => naming to docker.io/library/backend
```

E para executar a imagem:

```bash
❯ docker run -dp 3333:3333 backend
1916cbf87e00c9e436dcc7a6d9c80da9954b3cf5bb4c9bd9a3ffc204c359de2c
```

Após isso, o backend estará rodando localmente:

![Requisição 'GET /transactions/1' retornando uma transação em JSON com detalhes](./assets/transaction.png)

Para o frontend, temos um Dockerfile um pouco maior:

```docker
❯ cat front/Dockerfile
# ==== CONFIGURE =====
# Use a Node base image
FROM node:16.17.1 as build-stage

# Set the working directory to /usr/src/app inside the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# ==== BUILD =====
# Install dependencies
RUN npm install

# Copy files after install dependencies
COPY . .

# Build app
RUN npm run build

# ==== SERVE =====
# Use a nginx base image for serving content
FROM nginx:1.22 as production-stage

# Set working directory to nginx asset directory
WORKDIR /app

# Copy static assets from builder stage
COPY --from=build-stage /usr/src/app/build .

# Removes the default Nginx conf file from the image
RUN rm -rf /etc/nginx/conf.d/default.conf

# Copy nginx config
COPY nginx.conf /etc/nginx/nginx.conf

# Expose app
EXPOSE 80

# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
```

Esse Dockerfile é responsável por criar a imagem Docker para o frontend da aplicação. Ele é composto por 3 seções principais:

- **CONFIGURE**: define a imagem base, cria o diretório de trabalho e copia os arquivos `package.json` e `package-lock.json` para o diretório de trabalho.

- **BUILD**: instala as dependências do projeto com `RUN npm install`, copia todos os arquivos da aplicação para o diretório de trabalho e roda o comando `npm run build` para criar a build de produção.

- **SERVE**: define a imagem base do Nginx, define o diretório de trabalho como `/app`, copia os arquivos de build do diretório de trabalho do `build-stage`, remove o arquivo de configuração padrão do Nginx, copia o arquivo de configuração personalizado e expõe a porta `80`. Por fim, inicia o container do Nginx com o comando ENTRYPOINT ["nginx", "-g", "daemon off;"].

Existem estruturas muito parecidas com o `Dockerfile`, mas algumas novas. Uma delas é a ũtilização de dois comandos Docker `FROM`. Esta técnica é conhecida como **multi-stage build** e ela é usada no Docker para criar imagens mais leves e eficientes. Basicamente, a ideia é dividir o processo de construção em várias etapas (stages), onde cada stage é uma imagem Docker separada que é criada a partir da anterior.

Neste caso, o primeiro stage é responsável por construir o aplicativo React. Ele usa uma imagem do Node como base, copia os arquivos `package.json` e `package-lock.json`, instala as dependências do projeto e copia os arquivos do projeto. Em seguida, ele executa o comando `npm run build` para gerar os arquivos de build.

O segundo stage usa uma imagem do Nginx como base e copia os arquivos de build gerados pelo primeiro stage. Ele também copia o arquivo de configuração do Nginx e remove o arquivo de configuração padrão do Nginx da imagem e adiciona um arquivo de configuração personalizado que será explicado mais a seguir. Por fim, ele expõe a porta 80 e configura o Nginx como ponto de entrada.

Ao dividir o processo de construção em dois stages, a imagem final é mais leve e segura. Isso porque a imagem do primeiro stage, que é responsável pela construção do aplicativo, contém apenas as dependências necessárias para essa etapa, enquanto a imagem final do segundo stage contém apenas os arquivos de build e as configurações necessárias para executar o aplicativo.

> Em resumo, o Dockerfile do frontend cria uma imagem que inclui o servidor web Nginx e os arquivos de build da aplicação React para servir o conteúdo estático na porta 80.

De maneira similar, para construir a imagem docker do frontend e executá-la:

```bash
❯ docker build -t frontend ./front
[...]
❯ docker run -dp 80:80 frontend
c8e7be523704f8d084945c8bd7835fdf14db25f47b340345727866ebe79a9cbe
```

E a aplicação estará rodando localmente:

![Interface web com a aplicação frontend sendo executada em localhost](./assets/frontend.png)

Uma topologia para ajudar a visualizar a aplicação pensando apenas em uma imagem front e uma imagem back está representado abaixo:

![Topologia da aplicação docker](./assets/docker.png)

> A seguir, ao implementarmos esses serviços usando Kubernetes, perceberemos que, na verdade, essa topologia é mais complexa do que o exemplo apresentado. Porém, antes de prosseguirmos com a implementação no Kubernetes, vamos primeiro ver como armazenar essas imagens e usá-las em um pipeline.

## 🦊 GitLab

- Criação do projeto
- Utilização do registry
- Implementando pipeline de CI/CD com stages
- Uso de issues em boards
- Criação e uso de variáveis no pipeline

## ☸️ Kubernetes

- Gerenciamento de um cluster local
- Gerenciamento de um cluster remoto
- Práticas com os comandos kubectl
- Utilização do Deployment
- Utilização do ReplicaSet
- Utilização do Pod
- Utilização de Services ClusterIP e LoadBalancer

## 🟩 Nginx

- Configuração básica nginx.conf
- Passando solicitação para proxy server

## 🏗 Terraform

- Utilização do Terraform para provisionar a infraestrutura
- Leitura de documentação do Terraform
- Terraform para a Digital Ocean

## 🌊 Digital Ocean

- Familiarização com o Cloud Provider
- Ver métricas no terminal web

## 🌐 Configuração de DNS

- Apontar para nameservers
- Adicionar registro A para criar um subdomínio

## 🏁 Conclusão

- Lições aprendidas
- Próximos passos
