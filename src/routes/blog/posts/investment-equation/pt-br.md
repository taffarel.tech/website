---
title: Juros compostos com aportes
description: Esta é uma descrição em português
date: 2019-05-23
categories:
  - Investimentos
  - Matemática
author:
  id: 18634201
  name: Maurício Taffarel
  user: taffarel55
time: 20
published: true
---

# Equação de montante com aportes

$y[n+1]=(1+i)y[n]+x[n+1]$

$y[0]=C$

$x[n] = \tau\sum_{k=0}^\infty\delta[n-\rho(k+1)]$

Aplicando transformada Z:

$zY[z]-zy[0]=(1+i)Y[z]+zX[z]-zx[0]$

$zY[z]-zC=(1+i)Y[z]+zX[z]$

$zY[z]-(1+i)Y[z]=zC+zX[z]$

$Y[z]-\frac{(1+i)}{z}Y[z]=C+X[z]$

$Y[z](1-\frac{(1+i)}{z})=C+X[z]$

$Y[z](\frac{z-(1+i)}{z})=C+X[z]$

$Y[z]=C\frac{z}{z-(1+i)}+X[z]\frac{z}{z-(1+i)}$

$Y[z]=Y_{i0}[z]+Y_{s0}[z]$

$Y_{i0}[z]=C\frac{z}{z-(1+i)}$

$y_{i0}[n]=C(1+i)^{n}$



Exponencial e tals

$Y_{s0}[n]=X[z]\frac{z}{z-(1+i)}$

Bora ver $X[z]$

$x[n] = \tau\sum_{k=0}^\infty\delta[n-\rho(k+1)]$

$X[z]=\sum_{n=0}^\infty(\tau\sum_{k=0}^\infty\delta[n-\rho(k+1)])z^{-n}$

$X[z]=\tau\sum_{k=0}^\infty(\sum_{n=0}^\infty\delta[n-\rho(k+1)])z^{-n}$

$X[z]=\tau\sum_{k=0}^\infty z^{-\rho(k+1)}$



$Y_{s0}[z]=X[z]\frac{z}{z-(1+i)}$

$Y_{s0}[z]=\tau\sum_{k=0}^\infty \frac{z}{z-(1+i)}z^{-\rho(k+1)}$

$y_{s0}[n]=\tau\sum_{k=0}^\infty(1+i)^{n-\rho(k+1)}\space u[n-\rho(k+1)]$



###### tags: `Blog`
